# Get.Noticed I18n package

This package is a combined package that contains various language packs for Magento 2.
The language packs contain translations for many modules as well as the core
translations language pack we use for that language.

## Adding a new language pack

To add a new language pack, simply create a directory with the proper locale code inside of `i18n`.

Copy the `language.xml` and `registration.php` from any other language
and change the values to your new language pack.

## Adding new translations

To add new translations, CSV files MAY be added to any langauge pack directory (`i18n/<language_code>`).

When adding new translations, please keep the following guidelines in mind.

### Guidelines for adding new translations

*Note that these following statements adhere to [RFC2119](https://tools.ietf.org/html/rfc2119).*

* Theme translations MUST NOT be added to this package
* Customer specific translations MUST NOT be added to this package
* Store specific translations MUST NOT be added to this package
* The file names of any translation added MUST adhere to the standard of `<VendorName>_<ModuleName>.csv` , e.g. `GetNoticed_StoreNotice` or `Amasty_LayeredNavigation`. Another example: `i18n/de_DE/GetNoticed_StoreNotice.csv` contains the German translations for the `GetNoticed_StoreNotice` module.
* A translation file MUST NOT contain translations from multiple modules
* All translations SHOULD be quoted, e.g. `Yes,Ja` is not preferred, use `"Yes","Ja"` instead.