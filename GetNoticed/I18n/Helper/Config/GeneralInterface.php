<?php

namespace GetNoticed\I18n\Helper\Config;

interface GeneralInterface
{

    public function isModuleEnabled(): bool;

}