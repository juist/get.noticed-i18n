<?php

namespace GetNoticed\I18n\Helper\Config;

use GetNoticed\Common;
use Magento\Store;

class General
    extends Common\Helper\Config\AbstractConfigHelper
    implements GeneralInterface
{

    const XML_PATH_BASE = 'getnoticed_i18n/general';
    const XML_PATH_ENABLED = '%s/enabled';

    public function isModuleEnabled(): bool
    {
        return $this->scopeConfig->isSetFlag(
            sprintf(self::XML_PATH_ENABLED, self::XML_PATH_BASE),
            Store\Model\ScopeInterface::SCOPE_STORE,
            $this->storeManager->getStore()
        );
    }
    
}