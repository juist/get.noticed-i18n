<?php

namespace GetNoticed\I18n\VO;

use GetNoticed\I18n;

class ModuleInformationObject
    implements I18n\Api\Data\VO\ModuleInformationObjectInterface
{

    /**
     * @var string
     */
    protected $name;

    /**
     * @var string
     */
    protected $directoryPath;

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): I18n\Api\Data\VO\ModuleInformationObjectInterface
    {
        $this->name = $name;

        return $this;
    }

    public function getDirectoryPath(): string
    {
        return $this->directoryPath;
    }

    public function setDirectoryPath(string $directoryPath): I18n\Api\Data\VO\ModuleInformationObjectInterface
    {
        $this->directoryPath = $directoryPath;

        return $this;
    }

}