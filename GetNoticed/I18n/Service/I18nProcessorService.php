<?php

namespace GetNoticed\I18n\Service;

use Magento\Framework;
use GetNoticed\I18n;
use League\Csv;

class I18nProcessorService implements I18n\Api\I18nProcessorServiceInterface
{

    // Flags

    /**
     * @var bool
     */
    protected $writeNewOnly = true;

    // DI

    /**
     * @var Framework\Phrase\RendererInterface
     */
    protected $phraseRenderer;

    /**
     * @var Framework\TranslateInterface
     */
    protected $translator;

    public function __construct(
        Framework\Phrase\RendererInterface $phraseRenderer,
        Framework\TranslateInterface $translator
    ) {
        $this->phraseRenderer = $phraseRenderer;
        $this->translator = $translator;
    }

    /**
     * @inheritDoc
     */
    public function processCsv(
        Csv\Reader $inputCsv,
        Csv\Writer $outputCsv,
        string $outputCsvPath,
        array $headers = []
    ): void {
        $records = [];

        // Necessary for loading translations inside CLI
        $this->translator->loadData(Framework\App\Area::AREA_FRONTEND, false);
        Framework\Phrase::setRenderer($this->phraseRenderer);

        // Gather translations
        foreach ($inputCsv->getRecords($this->getHeaders($headers)) as $record) {
            if ($this->isWriteNewOnly() && $record['source'] !== __($record['source'])->render()) {
                continue;
            }

            $records[] = [$record['source'], '@TODO'];
        }

        $outputCsv->insertAll($records);

        \file_put_contents($outputCsvPath, trim($outputCsv->getContent()));
    }

    public function getHeaders(array $headers = []): array
    {
        if ($headers === null) {
            return ['source', 'translation'];
        }

        return $headers;
    }

    private function isWriteNewOnly()
    {
        return $this->writeNewOnly;
    }

    public function setWriteNewOnly(bool $writeNewOnly): void
    {
        $this->writeNewOnly = $writeNewOnly;
    }

}