<?php

namespace GetNoticed\I18n\Command;

use GetNoticed\I18n;

class CommandContext
{
    /**
     * @var I18n\Helper\Config\GeneralInterface
     */
    protected $generalSettings;

    /**
     * @var I18n\Api\I18nProcessorServiceInterface
     */
    protected $i18nProcessorService;

    public function __construct(
        I18n\Helper\Config\GeneralInterface $generalSettings,
        I18n\Api\I18nProcessorServiceInterface $i18nProcessorService
    ) {
        $this->generalSettings = $generalSettings;
        $this->i18nProcessorService = $i18nProcessorService;
    }

    /**
     * @return I18n\Helper\Config\GeneralInterface
     */
    public function getGeneralSettings(): I18n\Helper\Config\GeneralInterface
    {
        return $this->generalSettings;
    }

    /**
     * @return I18n\Api\I18nProcessorServiceInterface
     */
    public function getI18nProcessorService(): I18n\Api\I18nProcessorServiceInterface
    {
        return $this->i18nProcessorService;
    }
}