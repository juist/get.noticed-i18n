<?php

namespace GetNoticed\I18n\Command;

use GetNoticed\I18n;
use Symfony\Component\Console;

abstract class AbstractCommand extends Console\Command\Command
{
    /**
     * @var I18n\Helper\Config\GeneralInterface
     */
    protected $generalSettings;

    /**
     * @var I18n\Api\I18nProcessorServiceInterface
     */
    protected $i18nProcessorService;

    public function __construct(
        I18n\Command\CommandContext $context,
        ?string $name = null
    ) {
        parent::__construct($name);

        $this->generalSettings = $context->getGeneralSettings();
        $this->i18nProcessorService = $context->getI18nProcessorService();
    }

    public function isEnabled()
    {
        return $this->generalSettings->isModuleEnabled() !== true ? false : parent::isEnabled();
    }
}