<?php

namespace GetNoticed\I18n\Command;

use Magento\Framework;
use GetNoticed\I18n;
use Symfony\Component\Console;
use League\Csv;

class AutofillTranslatedPhrasesCommand
    extends I18n\Command\AbstractCommand
{

    const CMD_NAME = 'i18n:autofill-translated-phrases';
    const CMD_DESCRIPTION = 'This command takes the path to an existing CSV file as input and will attempt to fill in';

    const ARG_INPUT_CSV = 'input-csv';
    const ARG_OUTPUT_CSV = 'output-csv';

    // DI

    /**
     * @var Framework\App\State
     */
    protected $appState;

    /**
     * @var Framework\App\Filesystem\DirectoryList
     */
    protected $directoryList;

    /**
     * @var I18n\Api\I18nProcessorServiceInterface
     */
    protected $i18nProcessorService;

    public function __construct(
        Framework\App\State $appState,
        Framework\App\Filesystem\DirectoryList $directoryList,
        I18n\Api\I18nProcessorServiceInterface $i18nProcessorService,
        I18n\Command\CommandContext $context,
        ?string $name = self::CMD_NAME
    ) {
        parent::__construct($context, $name);

        $this->appState = $appState;
        $this->directoryList = $directoryList;
        $this->i18nProcessorService = $i18nProcessorService;
    }

    protected function configure()
    {
        $this->setDescription(self::CMD_DESCRIPTION);
        $this->setDefinition(
            new Console\Input\InputDefinition(
                [
                    new Console\Input\InputArgument(
                        self::ARG_INPUT_CSV,
                        Console\Input\InputArgument::REQUIRED,
                        'The CSV file with translations that should be autofilled.'
                    ),
                    new Console\Input\InputArgument(
                        self::ARG_OUTPUT_CSV,
                        Console\Input\InputArgument::REQUIRED,
                        'Where the result CSV should be saved.'
                    )
                ]
            )
        );
    }

    protected function execute(Console\Input\InputInterface $input, Console\Output\OutputInterface $output)
    {
        $inputCsvPath = $this->directoryList->getRoot()
            . DIRECTORY_SEPARATOR . $input->getArgument(self::ARG_INPUT_CSV);
        $inputCsv = Csv\Reader::createFromPath($inputCsvPath);
        $inputCsv->setHeaderOffset(0);

        $targetCsvPath = $this->directoryList->getRoot()
            . DIRECTORY_SEPARATOR . $input->getArgument(self::ARG_OUTPUT_CSV);
        $targetCsv = Csv\Writer::createFromString('');

        try {
            // To prevent league CSV from failing to open the target file,
            touch($targetCsvPath);

            $this->appState->emulateAreaCode(
                Framework\App\Area::AREA_FRONTEND,
                function (
                    Csv\Reader $inputCsv,
                    Csv\Writer $targetCsv,
                    string $targetCsvPath
                ) {
                    $this->i18nProcessorService->processCsv($inputCsv, $targetCsv, $targetCsvPath);
                },
                [$inputCsv, $targetCsv, $targetCsvPath]
            );

            $output->writeln(
                sprintf('<info>%s</info>', __('File is ready at: %1', $input->getArgument(self::ARG_OUTPUT_CSV)))
            );
        } catch (\Exception | \Error $e) {
            $output->writeln(sprintf('<error>%s</error>', __('An error occured: %1', $e->getMessage())));
        }
    }
}