<?php

namespace GetNoticed\I18n\Command;

use Magento\Framework;
use Magento\Setup;
use Magento\Setup\Module\I18n\Parser;
use GetNoticed\I18n;
use Symfony\Component\Console;
use League\Csv;

class FetchAllTranslateableFilesCommand
    extends I18n\Command\AbstractCommand
{

    const CMD_NAME = 'i18n:fetch-all-translateable-files';
    const CMD_DESCRIPTION = 'Fetches all translateable files and writes them to one directory into multiple files (per module/theme).';

    const ARG_OUTPUT_CSV_DIRECTORY = 'output-csv';

    // DI

    /**
     * @var Framework\App\State
     */
    protected $appState;

    /**
     * @var Framework\App\Filesystem\DirectoryList
     */
    protected $directoryList;

    /**
     * @var Framework\Module\Dir\Reader
     */
    protected $moduleReader;

    /**
     * @var Framework\Module\ModuleListInterface
     */
    protected $moduleList;

    /**
     * @var I18n\Api\Data\VO\ModuleInformationObjectInterfaceFactory
     */
    protected $moduleInformationObjectFactory;

    /**
     * @var Setup\Module\I18n\Dictionary\Generator
     */
    protected $dictionaryGenerator;

    public function __construct(
        Framework\App\State $appState,
        Framework\App\Filesystem\DirectoryList $directoryList,
        Framework\Module\Dir\Reader $moduleReader,
        Framework\Module\ModuleListInterface $moduleList,
        I18n\Api\Data\VO\ModuleInformationObjectInterfaceFactory $moduleInformationObjectFactory,
        I18n\Command\CommandContext $context,
        ?string $name = self::CMD_NAME
    ) {
        parent::__construct($context, $name);

        $this->appState = $appState;
        $this->directoryList = $directoryList;
        $this->moduleReader = $moduleReader;
        $this->moduleList = $moduleList;
        $this->moduleInformationObjectFactory = $moduleInformationObjectFactory;
    }

    protected function configure()
    {
        $this->setDescription(self::CMD_DESCRIPTION);
        $this->setDefinition(
            new Console\Input\InputDefinition(
                [
                    new Console\Input\InputArgument(
                        self::ARG_OUTPUT_CSV_DIRECTORY,
                        Console\Input\InputArgument::REQUIRED,
                        'Where the result CSV files should be written to (directory)'
                    )
                ]
            )
        );
    }

    protected function execute(Console\Input\InputInterface $input, Console\Output\OutputInterface $output)
    {
        $io = new Console\Style\SymfonyStyle($input, $output);

        try {
            // Get target directory
            $targetCsvDirectory = $this->getTargetCsvDirectory($input);

            // Gather all modules and their directory info
            $modules = $this->gatherModules();

            foreach ($modules as $module) {
                try {
                    $targetCsvFilePath = $targetCsvDirectory
                        . DIRECTORY_SEPARATOR . sprintf('%s.csv', $module->getName());
                    $this->getDictionaryGenerator()->generate($module->getDirectoryPath() . '/', $targetCsvFilePath);
                    $io->success(
                        __(
                            'Written CSV to %1 (source: %2)',
                            strtr($targetCsvFilePath, [$this->directoryList->getRoot() => '']),
                            strtr($module->getDirectoryPath(), [$this->directoryList->getRoot() => ''])
                        )
                    );
                } catch (\Exception | \Error $e) {
                    $io->error(
                        __(
                            'Failed to write to: %1 (source: %2) -> %3',
                            strtr($targetCsvFilePath, [$this->directoryList->getRoot() => '']),
                            strtr($module->getDirectoryPath(), [$this->directoryList->getRoot() => '']),
                            $e->getMessage()
                        )
                    );
                }
            }
        } catch (\Error $e) {
            $io->error(__('Error: %1', $e->getMessage()));
        } catch (\Exception $e) {
            $io->error(__('Exception: %1', $e->getMessage()));
        }
    }

    /**
     * @param \Symfony\Component\Console\Input\InputInterface $input
     *
     * @return string
     * @throws \GetNoticed\I18n\Exception\InvalidDirectoryException
     */
    protected function getTargetCsvDirectory(Console\Input\InputInterface $input): string
    {
        // Check target directory
        $targetCsvDirectory = $this->directoryList->getRoot()
            . DIRECTORY_SEPARATOR . $input->getArgument(self::ARG_OUTPUT_CSV_DIRECTORY);

        if (is_dir($targetCsvDirectory) !== true) {
            mkdir($targetCsvDirectory, 0777, true);
        }

        if (is_writable($targetCsvDirectory) !== true) {
            chmod($targetCsvDirectory, 0777);
        }

        if (is_writable($targetCsvDirectory) !== true) {
            throw I18n\Exception\InvalidDirectoryException::canNotWriteToDirectory($targetCsvDirectory);
        }

        return realpath($targetCsvDirectory);
    }

    /**
     * @return \GetNoticed\I18n\Api\Data\VO\ModuleInformationObjectInterface[]
     */
    private function gatherModules(): array
    {
        return array_map(
            function (array $moduleData) {
                $directoryPath = $this->moduleReader->getModuleDir(null, $moduleData['name']);

                return $this->getModuleInformationObjectInstance()
                            ->setName($moduleData['name'])
                            ->setDirectoryPath($directoryPath);
            },
            $this->moduleList->getAll()
        );
    }

    private function getModuleInformationObjectInstance(): I18n\Api\Data\VO\ModuleInformationObjectInterface
    {
        return $this->moduleInformationObjectFactory->create();
    }

    /**
     * Get dictionary generator
     *
     * @return \Magento\Setup\Module\I18n\Dictionary\Generator
     */
    private function getDictionaryGenerator()
    {
        if ($this->dictionaryGenerator !== null) {
            return $this->dictionaryGenerator;
        }

        $filesCollector = new Setup\Module\I18n\FilesCollector();

        $phraseCollector = new Parser\Adapter\Php\Tokenizer\PhraseCollector(
            new Parser\Adapter\Php\Tokenizer()
        );
        $adapters = [
            'php'  => new Parser\Adapter\Php($phraseCollector),
            'html' => new Parser\Adapter\Html(),
            'js'   => new Parser\Adapter\Js(),
            'xml'  => new Parser\Adapter\Xml(),
        ];

        $factory = new Setup\Module\I18n\Factory();
        $context = new Setup\Module\I18n\Context(new Framework\Component\ComponentRegistrar());
        $parser = new I18n\Module\I18n\Parser\Parser($filesCollector, $factory);
        $parserContextual = new Parser\Contextual($filesCollector, $factory, $context);
        foreach ($adapters as $type => $adapter) {
            $parser->addAdapter($type, $adapter);
            $parserContextual->addAdapter($type, $adapter);
        }

        return $this->dictionaryGenerator = new Setup\Module\I18n\Dictionary\Generator(
            $parser,
            $parserContextual,
            $factory,
            new Setup\Module\I18n\Dictionary\Options\ResolverFactory()
        );
    }

}