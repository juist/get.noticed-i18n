<?php

namespace GetNoticed\I18n\Command;

use Magento\Framework;
use Symfony\Component\Console;
use GetNoticed\I18n;
use League\Csv;

class ListDuplicatePhrasesCommand extends I18n\Command\AbstractCommand
{
    const CMD_NAME = 'i18n:list-duplicate-phrases';
    const CMD_DESCRIPTION = 'Lists all duplicate phrases within a single CSV file.';

    const ARG_CSV_FILE = 'csv-file';

    /**
     * @var Framework\App\Filesystem\DirectoryList
     */
    private $directoryList;

    public function __construct(
        I18n\Command\CommandContext $context,
        Framework\App\Filesystem\DirectoryList $directoryList,
        ?string $name = self::CMD_NAME
    ) {
        parent::__construct($context, $name);

        $this->directoryList = $directoryList;
    }

    protected function configure()
    {
        $this->setDescription(self::CMD_DESCRIPTION);
        $this->setDefinition(
            new Console\Input\InputDefinition(
                [
                    new Console\Input\InputArgument(
                        self::ARG_CSV_FILE,
                        Console\Input\InputArgument::REQUIRED,
                        'CSV file to check'
                    )
                ]
            )
        );
    }

    protected function execute(Console\Input\InputInterface $input, Console\Output\OutputInterface $output)
    {
        // IO (for communication)
        $io = new Console\Style\SymfonyStyle($input, $output);

        // Get csv file
        $csvPath = $this->directoryList->getRoot() . DIRECTORY_SEPARATOR . $input->getArgument(self::ARG_CSV_FILE);
        $readCsv = \fopen($csvPath, 'r');
        $translationKeys = [];
        $duplicates = [];

        while ($line = \fgetcsv($readCsv)) {
            $source = $line[0];
            $translation = $line[1];

            $hash = \sha1($source);

            if (in_array($hash, $translationKeys)) {
                if (array_key_exists($hash, $duplicates) !== true) {
                    $duplicates[$hash] = [
                        'source'       => $source,
                        'translations' => [],
                        'hash'         => $hash
                    ];
                }

                $duplicates[$hash]['translations'][] = $translation;
            }

            $translationKeys[] = $hash;
        }

        if (empty($duplicates)) {
            $io->success(__('No duplicates found!'));

            return;
        }

        $io->warning(__('Duplicates found, see list below.'));
        $io->table(
            ['Source', 'Translations'],
            array_map(
                function (array $item) {
                    return [
                        'Source'       => $item['source'],
                        'Translations' => implode(PHP_EOL, $item['translations'])
                    ];
                },
                $duplicates
            )
        );
    }
}