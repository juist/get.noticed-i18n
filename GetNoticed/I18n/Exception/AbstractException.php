<?php

namespace GetNoticed\I18n\Exception;

use Magento\Framework\Exception;

abstract class AbstractException
    extends Exception\LocalizedException
{

}