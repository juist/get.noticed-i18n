<?php

namespace GetNoticed\I18n\Exception;

class InvalidDirectoryException
    extends AbstractException
{

    public static function canNotWriteToDirectory(string $targetDirectory)
    {
        return new self(__('Unable to write to directory: %1', $targetDirectory));
    }

}