<?php

namespace GetNoticed\I18n\Test;

interface TestCaseInterface
{

    public function getTestDirBase();

    public function getTestDirFiles();

    public function create($type, $arguments = []);

}