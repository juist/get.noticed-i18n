# Magento 2 - GetNoticed I18n

{$warningMessage}

## Unit Tests

## Setup Unit Tests

1. Rename $MAGENTO_ROOT/dev/tests/unit/phpunit.xml.dist to phpunit.xml
2. Add test in phpunit.xml

```
<testsuite name="I18n">
    <directory suffix="Test.php">../../../app/code/GetNoticed/I18n/Test/Unit</directory>
    <directory suffix="Test.php">../../../vendor/GetNoticed/I18n/Test/Unit</directory>
</testsuite>
```

### Running Unit Tests

Execute bash on running container.

```
cd $MAGENTO_ROOT/dev/tests/unit/
../../../vendor/bin/phpunit
```

### View Unit Tests

Execute bash on running container.

```
cd $MAGENTO_ROOT/dev/tests/unit/
../../../vendor/bin/phpunit --suite I18n --testdox
```

## Integration Tests

### Setup Integration Tests

1. Rename $MAGENTO_ROOT/dev/tests/integration/phpunit.xml.dist to phpunit.xml
2. Add test in phpunit.xml

```
<testsuite name="I18n">
    <directory suffix="Test.php">../../../app/code/GetNoticed/I18n/Test/Integration</directory>
    <directory suffix="Test.php">../../../vendor/GetNoticed/I18n/Test/Integration</directory>
</testsuite>
```

### Running Integration Tests

Execute bash on running container.

```
cd $MAGENTO_ROOT/dev/tests/integration/
../../../vendor/bin/phpunit --suite I18n
```

### View Integration Tests

Execute bash on running container.

```
cd $MAGENTO_ROOT/dev/tests/integration/
../../../vendor/bin/phpunit --testdox
```

# Test Results

****
## Test {$passedText} - {$date}
****

### Tests

#### Unit Tests

```
{$listTestOutputUnit}
```
#### Integration Tests

```
{$listTestOutputIntegration}
```

### Test results

#### Unit test results

```
{$testResultOutputUnit}
```

#### Integration test results

```
{$testResultOutputIntegration}
```