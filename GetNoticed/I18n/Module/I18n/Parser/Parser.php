<?php

namespace GetNoticed\I18n\Module\I18n\Parser;

use Magento\Setup;

class Parser
    extends Setup\Module\I18n\Parser\Parser
{

    /**
     * @inheritDoc
     */
    public function parse(array $parseOptions)
    {
        $this->_phrases = [];

        return parent::parse($parseOptions);
    }

}