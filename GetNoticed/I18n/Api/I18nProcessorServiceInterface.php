<?php

namespace GetNoticed\I18n\Api;

use League\Csv;

interface I18nProcessorServiceInterface
{

    /**
     * @param \League\Csv\Reader $inputCsv
     * @param \League\Csv\Writer $outputCsv
     *
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws \Exception
     */
    public function processCsv(
        Csv\Reader $inputCsv,
        Csv\Writer $outputCsv,
        string $outputCsvPath
    ): void;

    public function getHeaders(array $headers = []): array;
}