<?php

namespace GetNoticed\I18n\Api\Data\VO;

use GetNoticed\I18n;

interface ModuleInformationObjectInterface
{

    public function getName(): string;

    public function setName(string $name): I18n\Api\Data\VO\ModuleInformationObjectInterface;

    public function getDirectoryPath(): string;

    public function setDirectoryPath(string $directoryPath): I18n\Api\Data\VO\ModuleInformationObjectInterface;

}